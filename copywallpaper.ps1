#moves wallpapper and installs

# Define the owner account/group
# files are originally owned by TrustedInstaller so admin cannot del the file.  this is the workaround
$Account = New-Object -TypeName System.Security.Principal.NTAccount -ArgumentList 'BUILTIN\Administrators';

# Get a list of folders and files , need to reset permissions
$ItemList = Get-ChildItem -Path c:\windows\web\wallpaper\windows\ -Recurse;

# Iterate over files/folders
foreach ($Item in $ItemList) {
    $Acl = $null; # Reset the $Acl variable to $null
    $Acl = Get-Acl -Path $Item.FullName; # Get the ACL from the item
    $Acl.SetOwner($Account); # Update the in-memory ACL
   # Set-Acl -Path $Item.FullName -AclObject $Acl;  # Set the updated ACL on the target item

    $rule=new-object System.Security.AccessControl.FileSystemAccessRule ("BUILTIN\Administrators","FullControl","Allow")
    $Acl.SetAccessRule($rule)

    Set-ACL -Path $Item.FullName -AclObject $Acl
}

Copy-Item C:\Windows\falcon\vdevboxAmi-master\wallpaper.jpg C:\Windows\Web\Wallpaper\Windows\img0.jpg