#launches a command at jumpcloud
#but everything goes well may not have to use it

$APIkey = ''
$triggerName = 'falconStartupTrigger'
$TriggerURL = "https://console.jumpcloud.com/api/command/trigger/$triggerName"

$hdrs = @{}
$hdrs.Add("Accept","application/json")
$hdrs.Add("X-API-KEY","$APIkey")

Invoke-RestMethod -Method POST -Uri $TriggerURL -Header $hdrs

