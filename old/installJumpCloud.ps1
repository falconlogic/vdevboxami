﻿#dont presently need to run a vbs on every startup
#rather than be in startup and having to call a trigger
#i can prob just run commands from jumpcloud to all systems if I wanted to 
#Set-ItemProperty "HKLM:\Software\Microsoft\Windows\CurrentVersion\Run" -Name 'CallJumpCloud' -Value "c:\WINDOWS\falconlogic.vbs"
#New-Item -Path c:\test3 -ItemType directory

#install jumpcloud
(New-Object System.Net.WebClient).DownloadFile("https://s3.amazonaws.com/jumpcloud-windows-agent/production/versions/0.38.1/JumpCloudInstaller.exe", "c:\windows\JumpCloudInstaller.exe")
c:\windows\JumpCloudInstaller.exe -k ea3c131c1f240360d6d090bddba824539bc743b4 /VERYSILENT /NORESTART /SUPPRESSMSGBOXES | Out-Null

#install cmd-lets
Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
Install-Module JumpCloud -Scope CurrentUser -FORCE

$APIkey = ''

#add system to the virtualDevBox group
Connect-JCOnline $APIkey -force
# we select last one created.  so as long as your not creating them faster than every 5 min should be fine
$output = Get-JCSystem | Select _id | select -Last 1
$sysID = $output._id
Add-JCSystemGroupMember -GroupName virtualDevBoxes -SystemID $sysID

$triggerName = 'falconSetup'
$TriggerURL = "https://console.jumpcloud.com/api/command/trigger/$triggerName"

$hdrs = @{}
$hdrs.Add("Accept","application/json")
$hdrs.Add("X-API-KEY","$APIkey")

Invoke-RestMethod -Method POST -Uri $TriggerURL -Header $hdrs


logoff



