﻿#only local admin can run hklm else it will not work.  so building into default user before sys prep
Set-ItemProperty -path 'HKLM:\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer' -Name 'UseDefaultTile' -Type DWord -Value "1"
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\ActiveDesktop' -Name NoChangingWallPaper -Value 1 -Force
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Personalization' -Name LockScreenImage -Value "$env:windir\falcon\vdevboxami-master\wallpaper.jpg" -Force
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Personalization' -Name NoChangingLockScreen -Value 1 -Force

Set-ItemProperty -Path 'HKCU:\Control Panel\Desktop' -Name Wallpaper -Value "$env:windir\falcon\vdevboxami-master\wallpaper.jpg" -Force
Set-ItemProperty -Path 'HKCU:\Control Panel\Desktop' -Name SCRNSAVE.EXE -Value C:\Windows\system32\ssText3d.scr -Force
Set-ItemProperty -Path 'HKCU:\Control Panel\Desktop' -Name ScreenSaveTimeOut -Value 1566 -Force
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Screensavers\ssText3d' -Name DisplayString -Value 'Falcon Logic' -Force
Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Accent' -Name StartColorMenu -Value ff52443b -Force
Set-ItemProperty -Path 'HKCU:\Software\Microsoft\ServerManager' -Name CheckedUnattendLaunchSetting -Value 0 -Force

